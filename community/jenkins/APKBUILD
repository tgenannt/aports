# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=jenkins
pkgver=2.267
pkgrel=0
pkgdesc="Extendable continuous integration server (stable version)"
url="https://jenkins.io"
arch="noarch !mips !mips64"
license="MIT"
depends="openjdk8 openjdk8-jre openjdk8-jre-base
	freetype ttf-dejavu xvfb fontconfig"
install="$pkgname.pre-install"
options="!check"
pkgusers="$pkgname"
pkggroups="$pkgname"
subpackages="$pkgname-openrc"
source="$pkgname-$pkgver.war::http://mirrors.jenkins.io/war/$pkgver/jenkins.war
	$pkgname.logrotate
	$pkgname.initd
	$pkgname.confd"
builddir="$srcdir/"

# secfixes:
#   2.245-r0:
#     - CVE-2020-2220
#     - CVE-2020-2221
#     - CVE-2020-2222
#     - CVE-2020-2223
#   2.228-r0:
#     - CVE-2020-2160
#     - CVE-2020-2161
#     - CVE-2020-2162
#     - CVE-2020-2163

package() {
	install -Dm755 "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd \
		"$pkgdir"/etc/conf.d/$pkgname
	install -Dm755 "$srcdir"/$pkgname-$pkgver.war \
		"$pkgdir"/usr/share/webapps/jenkins/$pkgname.war
	install -Dm644 "$srcdir"/$pkgname.logrotate \
		"$pkgdir"/etc/logrotate.d/$pkgname
	mkdir -p "$pkgdir"/var/log/jenkins
	chown -R $pkgusers:$pkggroups "$pkgdir"/var/log/jenkins
}

sha512sums="39c042b3ca9d48c040d0a9d9ac9d65aff2f579888e24b800bc1e1b77b7175528aed31f2675b3417577b0b0eef7c17b6faa2d7b794360a17a6a4c8ba867007185  jenkins-2.267.war
74423d3c66e2312eb3a1590e0582ccd82fc01b410d3bfc0627bef56fe6f4e7f4ea01a7a2d92a7a0c4870a1a1c48e911fe7eab3073e14db4910b52158182e5856  jenkins.logrotate
43686a537248c7a0a8fe53c3ca9577c8ffb50a141248de028d398d0fd3b3be8562b6cb2c63b44b3b0ac58d6431e8907790553791b2e125d1bfc2e3263ffaa83e  jenkins.initd
7247750a13fc2537dc1e405f6d8221ccdc80cfbaf40c47327ee04c206afa8607ada52e7b895c8eb3489dd9f6a94b42b8b38110b3120948a35dc4f197fe4c08ed  jenkins.confd"
